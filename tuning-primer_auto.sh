#!/bin/bash

CONF_FILE="${HOME}/.my.cnf"
echo '[client]' > "${CONF_FILE}"

if [ -n "$MYSQL_PORT_3306_TCP_ADDR" ] ; then
    socat UNIX-LISTEN:/tmp/mysql.sock,fork,reuseaddr,unlink-early,user=mysql,group=mysql,mode=777 \
        TCP:$MYSQL_PORT_3306_TCP_ADDR:3306 &
    echo "socket=/tmp/mysql.sock" >> "${CONF_FILE}"
   #echo "host=$MYSQL_PORT_3306_TCP_ADDR" >> "${CONF_FILE}"
fi

if [ -n "$MYSQL_PORT_3306_TCP_PORT" ] ; then
   echo "port=$MYSQL_PORT_3306_TCP_PORT" >> "${CONF_FILE}"
fi

if [ -n "$MYSQL_ENV_MYSQL_ROOT_PASSWORD" ] ; then
   echo "user=root" >> "${CONF_FILE}"
   echo "password=$MYSQL_ENV_MYSQL_ROOT_PASSWORD" >> "${CONF_FILE}"
fi
echo ""
echo "configuration file:"
echo ""
cat "${CONF_FILE}"
echo ""
echo "tuning-primer $ARGS $*"
# shellcheck disable=SC2086,SC2048
/usr/local/bin/tuning-primer ${ARGS} $*
