all: test build

shellcheck_exists:
	@shellcheck -V &> /dev/null

docker_exists:
	@docker version &>/dev/null

docker_compose_exists:
	@docker-compose version &>/dev/null

shellcheck: shellcheck_exists
	# shellcheck
	shellcheck -x tuning-primer_auto.sh
	shellcheck -x run.sh

dockerfile_lint: docker_exists
	docker run -it --rm --privileged -v `pwd`:/root/ projectatomic/dockerfile-lint dockerfile_lint -f Dockerfile

test: shellcheck dockerfile_lint

build:
	docker build -t katta/mysql-tuning-primer .

run:
	sh run.sh
