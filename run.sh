#!/bin/bash
MYSQL_CONTAINER="${1:-mysql}"

docker run -ti \
  --rm \
  --link "${MYSQL_CONTAINER}:mysql" \
  -v "${PWD}:/data" \
  katta/mysql-tuning-primer
